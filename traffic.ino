/* 

TO DO LIST voor de volgende assesment

- Geen delays, als de lamp verwisselt van rood -> groen of andersom is er een delay waarbij de oranje lamp brandt. 
De delay moet er uit, maar de oranje lamp moet nog branden. 

- Wanneer op de buttons wordt gedrukt, moet er niet gewacht worden voor de nieuwe cyclus de desbetreffende lamp moet zo snel mogelijk groen branden ( Als die op rood staat ! )

- Implementeren van "groene golf", dat is een systeem waarbij als een auto constant 50km/h rijdt dat die nooit wacht op rood licht. 
hierbij moeten er gesimuleert worden, dat de arduino vanuit een andere arduino die "soortvan" is verbonden een bericht ontvangt. 
In de console moet de user twee parameters invoeren, zowel de snelheid en het afstand. Vanuit de snelheid en afstand moet de interval aangepast worden.      
      
*/





#define LONG_INTERVAL 15000 //butten wel gedrukt 
#define SHORT_INTERVAL 10000 // button niet gedrukt 

#define znButton 14
#define woButton 15
#define nzButton 16
#define owButton 17

#define zuidRed 2
#define zuidYellow 13
#define zuidGreen 12

#define noordRed 11
#define noordYellow 10
#define noordGreen 9

#define westRed 6
#define westYellow 7
#define westGreen 8

#define oostRed 5
#define oostYellow 4
#define oostGreen 3

//enum begint met 0,1,2 etc.
enum trafficLights {
  NOORDZUID,
  OOSTWEST,
  //add meer stoplichten hier
  NUM_TRAFFIC_LIGHTS, //gebruikt voor count
};

enum kleur {
  RED,
  YELLOW,
  GREEN,
};

bool buttons_used[NUM_TRAFFIC_LIGHTS]; /// Array index met totaal 2 elementen

int changeDelay = 1000;

int loop_interval = SHORT_INTERVAL; //wordt gebruikt om te bepalen hoe lang een licht groen moet zijn
unsigned long currentMillis;
unsigned long previousMillis = 0;
long cycleTime;
//cycle time voor debug

//stoplicht status, true = groen
boolean trafficWO = false;
boolean trafficNZ = false;

//cycle door alle stoplichten, dit is index
int nextTrafficLightOnGreen = NOORDZUID;

//wordt aangeroepen bij druk op knop noord of knop zuid
void buttonEventNZ() {
  //if false set true
  if (buttons_used[NOORDZUID] == false) {
    buttons_used[NOORDZUID] = true;
    Serial.println("nz button");
  }
}

//wordt aangeroepen bij druk op knop oost of knop west
void buttonEventWO() {
  //if false set true
  if (buttons_used[OOSTWEST] == false) {
    buttons_used[OOSTWEST] = true;
    Serial.println("wo button");
  }
}

void setEastWestGreen() {
  if (trafficWO == false )
  {
    trafficWO = true;
    trafficNZ = false;
    //set more lights false here
    setNoordZuid(RED);
    setOostWest(GREEN);
  }
}

void setNorthSouthGreen() {
  if (trafficNZ == false)
  {
    trafficNZ = true;
    trafficWO = false;
    //set more lights false here
    setOostWest(RED);
    setNoordZuid(GREEN);
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println("millis \t previous \t cycleTime");
  trafficWO = false;
  trafficNZ = false;
  //set all lights red
  setTrafficPinmodes();
  ledSetStart();
}

void loop() {
  // put your main code here, to run repeatedly:
  currentMillis = millis(); //de tijd begint

  //Wanneer button is ingedrukt, wordt het geregisteerd.
  if (digitalRead(znButton) == HIGH || digitalRead(nzButton) == HIGH ) {
    buttonEventNZ();
  }
  if (digitalRead(woButton) == HIGH || digitalRead(owButton) == HIGH) {
    buttonEventWO();
  }
  //add more buttons here



  

  //is het tijd om wat te doen?
  if ((long) (currentMillis - previousMillis) >= loop_interval)
  {
    //cycletime is voor debug
    cycleTime = (long)(currentMillis - previousMillis);
    //previous time onthouden voor de volgende loop
    previousMillis = currentMillis;


    /* button ingedrukt: 15 seconden groen, en daarna wordt de button weer gereset.
      button niet gedrukt, 10 seconden groenlicht
      nextTrafficLightOnGreen wordt gebruikt als array index, om op zoek naar de button die bij current hoort.
    */
    if (buttons_used[nextTrafficLightOnGreen])
    {
      loop_interval = LONG_INTERVAL;
      buttons_used[nextTrafficLightOnGreen] = false; // button wordt gereset. dit zorgt ervoor dat de interval van long naar short veranderd, voor de volgende loop.
    }
    else
    {
      loop_interval = SHORT_INTERVAL;
    }

    switch (nextTrafficLightOnGreen ++) // na elk loop interval wordt de index verhoogd
    {
      default:
      case NOORDZUID:
            setNorthSouthGreen();
        break;
      case OOSTWEST:
        setEastWestGreen();
        break;
        //add meerdere cases (trafficlights) hier.
    };
    //index wordt gereset als de max is behaald( Max is NUM_TRAFFIC_LIGHTS = 2 )
    if (nextTrafficLightOnGreen == NUM_TRAFFIC_LIGHTS) nextTrafficLightOnGreen = 0;

    //print loop times
    Serial.print(currentMillis);
    Serial.print("\t");
    Serial.print(previousMillis);
    Serial.print("\t\t");
    Serial.println(cycleTime);
  }
  
}

void setTrafficPinmodes()
{
  //Set button to input
  pinMode(znButton, INPUT);
  pinMode(woButton, INPUT);
  pinMode(nzButton, INPUT);
  pinMode(owButton, INPUT);
  //-----------------------------

  //set LED pinmodes
  pinMode(zuidRed, OUTPUT);
  pinMode(zuidYellow, OUTPUT);
  pinMode(zuidGreen, OUTPUT);

  pinMode(noordRed, OUTPUT);
  pinMode(noordYellow, OUTPUT);
  pinMode(noordGreen, OUTPUT);

  pinMode(westRed, OUTPUT);
  pinMode(westYellow, OUTPUT);
  pinMode(westGreen, OUTPUT);

  pinMode(oostRed, OUTPUT);
  pinMode(oostYellow, OUTPUT);
  pinMode(oostGreen, OUTPUT);
  //------------------------------
}

void ledSetStart()
{
  digitalWrite(zuidRed, HIGH);
  digitalWrite(zuidGreen, LOW);
  digitalWrite(zuidYellow, LOW);

  digitalWrite(noordRed, HIGH);
  digitalWrite(noordGreen, LOW);
  digitalWrite(noordYellow, LOW);


  digitalWrite(westRed, HIGH);
  digitalWrite(westGreen, LOW);
  digitalWrite(westYellow, LOW);

  digitalWrite(oostRed, HIGH);
  digitalWrite(oostGreen, LOW);
  digitalWrite(oostYellow, LOW);
}

///////////////////
///////////////////
//////////////////
void setNoordZuid(int kleur)
{
  // zet noord en zuid op rood of groen
  if (kleur == RED)
  {
    //zet op rood
    digitalWrite(zuidGreen, LOW);
    digitalWrite(noordGreen, LOW);

    digitalWrite(zuidYellow, HIGH);
    digitalWrite(noordYellow, HIGH);

    delay(changeDelay);

    digitalWrite(zuidYellow, LOW);
    digitalWrite(noordYellow, LOW);

    digitalWrite(zuidRed, HIGH);
    digitalWrite(noordRed, HIGH);

  }
  if (kleur == GREEN)
  {
    //zet op groen

    digitalWrite(zuidYellow, HIGH);
    digitalWrite(noordYellow, HIGH);

    delay(changeDelay);

    digitalWrite(zuidYellow, LOW);
    digitalWrite(noordYellow, LOW);

    digitalWrite(zuidRed, LOW);
    digitalWrite(noordRed, LOW);

    digitalWrite(zuidGreen, HIGH);
    digitalWrite(noordGreen, HIGH);
  }
}

void setOostWest(int kleur)
{
  // zet oost en west op rood en groen
  if (kleur == RED)
  {
    //zet op rood
    digitalWrite(oostGreen, LOW);
    digitalWrite(westGreen, LOW);

    digitalWrite(oostYellow, HIGH);
    digitalWrite(westYellow, HIGH);

    delay(changeDelay);

    digitalWrite(oostYellow, LOW);
    digitalWrite(westYellow, LOW);

    digitalWrite(oostRed, HIGH);
    digitalWrite(westRed, HIGH);

  }
  if (kleur == GREEN)
  {
    //zet op groen
    digitalWrite(oostYellow, HIGH);
    digitalWrite(westYellow, HIGH);

    delay(changeDelay);

    digitalWrite(oostYellow, LOW);
    digitalWrite(westYellow, LOW);

    digitalWrite(oostRed, LOW);
    digitalWrite(westRed, LOW);

    digitalWrite(oostGreen, HIGH);
    digitalWrite(westGreen, HIGH);
  }
}